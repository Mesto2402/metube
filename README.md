# MeTube



## Previous requirements

After start this proyect, you need to have [Node.js](https://nodejs.org/es/download/) in your computer.

### Download Node.js
1) Go to the [download page of Node.js](https://nodejs.org/es/download/).
2) Select the SO that your computer have and press Download.
3) Execute the .exe file and follow the steps.
4) Press finish.

### Verify if you have Node.js
#### Windows
1) Press Windows + R if you use the SO of Windows
2) Write **cmd** in the input file.
3) The command window is open.
4) Write ` node --version ` and if you see a version it's all correct.

#### Linux
1) Search **Command window** in the explorer of the SO.
2) The command window is open.
3) Write ` node --version ` and if you see a version it's all correct.

If you have any problem, please consult how to install for [Linux](https://www.youtube.com/watch?v=OMhMnj7SBRQ) and [Windows](https://www.youtube.com/watch?v=qYwLOXjAiwM)

---

# APIs

In this part, you can see the description of each API that is implemented in this proyect divided in GET and POST APIs.

## GET

### Captions
Returns a list of subtitle tracks that are associated with a specific video, in this case, it refers to the video whose ID is **2MKkj1DQ0NU**.

>https://test-proyect-umg.herokuapp.com/captions/2MKkj1DQ0NU

| Key | Type | Description | 
| :--- | :---- | :---- |
| key | string* | API Key used to access YouTube's APIs. |
| part | string* | Specifies the parts of resources that the API response will include.The following list contains the names that you can include in the parameter value: id, **snippet**|

----

### ChannelSections
Returns a list of the sections of a channel, in this case, the channel with the ID **UCJWDdJyZROXdKY22BkwYNUQ** was used.

>https://test-proyect-umg.herokuapp.com/channel-sections/UCJWDdJyZROXdKY22BkwYNUQ

| Key | Type | Description | 
| :--- | :---- | :---- |
| key | string* | API Key used to access YouTube's APIs. |
| part | string* | Specifies the parts of resources that the API response will include.The following list contains the names that you can include in the parameter value: id, **snippet**,contentDetails|
| channelId | string* | Specify a YouTube channel ID. If a request specifies a value for this parameter, the API will only return the sections of the specified channel. |

---

### I18nRegions
Returns a list of content regions that the YouTube website supports.

>https://test-proyect-umg.herokuapp.com/i18nRegions

| Key | Type | Description | 
| :--- | :---- | :---- |
| key | string* | API Key used to access YouTube's APIs. |
| part | string* | Specifies the parts of resources that the API response will include.The following list contains the names that you can include in the parameter value: **snippet** |

---

### PlaylistItems
It shows a collection of elements of a playlist that matches the API request parameters, in this case, it refers to the playlist whose ID is **PLillGF-RfqbbI5_XFuidhe3EE7bej5T_y**.

>https://test-proyect-umg.herokuapp.com/playlist-items/PLillGF-RfqbbI5_XFuidhe3EE7bej5T_y

| Key | Type | Description | 
| :--- | :---- | :---- |
| key | string* | API Key used to access YouTube's APIs. |
| part | string* | Specifies the parts of resources that the API response will include.The following list contains the names that you can include in the parameter value: id, **snippet**, contentDetails, status. |
| playlistId | string* | Specifies the unique ID of the playlist for which you want to retrieve the playlist items. |

---

### VideoCategories
Displays a list of the categories that can be associated with YouTube videos, in this case, the categories from Mexico.

>https://test-proyect-umg.herokuapp.com/video-categories/MX

| Key | Type | Description | 
| :--- | :---- | :---- |
| key | string* | API Key used to access YouTube's APIs. |
| part | string* | Specifies the parts of resources that the API response will include.The following list contains the names that you can include in the parameter value: id, **snippet** |
| regionCode | string* | The regionCodele parameter tells the API to display the list of video categories available in the specified country. |

--- 

## POST

### Search
Returns a set of search results that match the **search** parameter for the API request.

>https://test-proyect-umg.herokuapp.com/search

| Key | Type | Description | 
| :--- | :---- | :---- |
| key | string* | API Key used to access YouTube's APIs. |
| part | string* | Specifies the parts of resources that the API response will include.The following list contains the names that you can include in the parameter value: id, **snippet** |
| search | string | Specifies the query term to search for. |

---

### Playlists 
Returns a collection of playlists that match the **channelId** parameter for the API request.

>https://test-proyect-umg.herokuapp.com/playlists

| Key | Type | Description | 
| :--- | :---- | :---- |
| key | string* | API Key used to access YouTube's APIs. |
| part | string* | Specifies the parts of resources that the API response will include.The following list contains the names that you can include in the parameter value: id, **snippet**, status |
| channelId | string* | This value indicates that the API should only display the playlists of the specified channel. |

---

### Channel  
Returns a collection of zero or more channel resources that match the **id** parameter for the API request.

>https://test-proyect-umg.herokuapp.com/channel

| Key | Type | Description | 
| :--- | :---- | :---- |
| key | string* | API Key used to access YouTube's APIs. |
| part | string* | Specifies the parts of resources that the API response will include.The following list contains the names that you can include in the parameter value: id, **snippet**, brandingSettings, contentDetails, invideoPromotion, statistics, topicDetails. |
| id | string* | Specifies the YouTube channel ID of the channel. |

---

### VideoCategories  
Displays a list of categories that can be associated with YouTube videos depending on the **regionCode** parameter for the API request.

>https://test-proyect-umg.herokuapp.com/videoCategories

| Key | Type | Description | 
| :--- | :---- | :---- |
| key | string* | API Key used to access YouTube's APIs. |
| part | string* | Specifies the parts of resources that the API response will include.The following list contains the names that you can include in the parameter value: id, **snippet**. |
| regionCode | string* | Tells the API to display the list of video categories available in the specified country. |

---

### Activities  
Displays a list of channel activity events that match the **channelId** parameter for the API request.

>https://test-proyect-umg.herokuapp.com/activities

| Key | Type | Description | 
| :--- | :---- | :---- |
| key | string* | API Key used to access YouTube's APIs. |
| part | string* | Specifies the parts of resources that the API response will include.The following list contains the names that you can include in the parameter value: id, **snippet**, contentDetails. |
| channelId | string* | Specify a unique YouTube channel ID. When set, the API shows the activities for that channel. |