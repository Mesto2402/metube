require('dotenv').config();
const { google } = require('googleapis')
const express = require('express')
const morgan = require('morgan')

let key = process.env.YOUTUBE_TOKEN;

// Using packages
const app = express()

// Set port for APP
let port = process.env.PORT || 8080

// Middleware
app.use(express.json())
app.use(morgan('dev'))

// Definining routes
app.get('/', (req, res) => {
    res.send("<h1>Hello to MeTube!</h1>");
})

// GET Methods
// Captions API
app.get('/captions/2MKkj1DQ0NU', (req, res) => {
    google.youtube('v3').captions.list({
        key: key,
        part: 'snippet',
        videoId: "2MKkj1DQ0NU"
    }).then((response) => {
        res.send(response.data.items)
    }).catch((err) => res.status(404).json(err));
})

// ChannelSections API
app.get('/channel-sections/UCJWDdJyZROXdKY22BkwYNUQ', (req, res) => {
    google.youtube('v3').channelSections.list({
        key: key,
        part: 'snippet',
        channelId: "UCJWDdJyZROXdKY22BkwYNUQ"
    }).then((response) => {
        res.send(response.data.items)
    }).catch((err) => res.status(404).json(err));
})

// I18nRegions API
app.get('/i18nRegions', (req, res) => {
    google.youtube('v3').i18nRegions.list({
        key: key,
        part: 'snippet'
    }).then((response) => {
        res.send(response.data.items)
    }).catch((err) => res.status(404).json(err));
})

// PlaylistItems API
app.get('/playlist-items/PLillGF-RfqbbI5_XFuidhe3EE7bej5T_y', (req, res) => {
    google.youtube('v3').playlistItems.list({
        key: key,
        part: 'snippet',
        playlistId: 'PLillGF-RfqbbI5_XFuidhe3EE7bej5T_y'
    }).then((response) => {
        res.send(response.data.items)
    }).catch((err) => res.status(404).json(err));
})

// VideoCategories API
app.get('/video-categories/MX', (req, res) => {
    google.youtube('v3').videoCategories.list({
        key: key,
        part: 'snippet',
        regionCode: 'MX'
    }).then((response) => {
        res.send(response.data.items)
    }).catch((err) => res.status(404).json(err));
})

// POST Methods
// Search API
app.post('/search', (req, res) => {
    const { search } = req.body
    google.youtube('v3').search.list({
        key: key,
        part: 'snippet',
        q: search
    }).then((response) => {
        if(search == undefined || search == null || search == "")
            res.status(404).send("Hubo un error, verificar que la variable se llame search o que no lo este vacio o nulo")
        res.send(response.data.items)
    }).catch((err) => res.status(404).json(err));
})

// Playlists API
app.post('/playlists', (req,res) => {
    const { channelId } = req.body
    google.youtube('v3').playlists.list({
        key: key,
        part: 'snippet',
        channelId: channelId
    }).then((response) => {
        res.send(response.data)
    }).catch((err) => res.status(404).send(err.response.data));
})

// Channel API
app.post('/channel', (req,res) => {
    const { id } = req.body
    google.youtube('v3').channels.list({
        key: key,
        part: 'snippet',
        id: id
    }).then((response) => {
        if(response.data.pageInfo.totalResults == 0)
            res.status(404).send("No se encontró ningun canal con el ID ingresado")
        res.send(response.data)
    }).catch((err) => res.status(404).send(err.response.data));
})

// VideoCategories API
app.post('/videoCategories', (req,res) => {
    const { regionCode } = req.body
    google.youtube('v3').videoCategories.list({
        key: key,
        part: 'snippet',
        regionCode: regionCode
    }).then((response) => {
        res.send(response.data)
    }).catch((err) => res.status(404).send(err.response.data));
})

// Activities API
app.post('/activities', (req,res) => {
    const { channelId } = req.body
    google.youtube('v3').activities.list({
        key: key,
        part: 'snippet',
        channelId: channelId
    }).then((response) => {
        if(response.data.pageInfo.totalResults == 0)
            res.status(404).send("No se encontró ninguna actividad con el ID ingresado")
        res.send(response.data)
    }).catch((err) => res.status(404).send(err.response.data));
})

// Listener
app.listen(port, () => {
    console.log('Port: ' + port);
})